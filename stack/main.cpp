#include <iostream>
#include "./stack.h"

void convert(Stack<char>& S, int n, int base){
  static char digit[] = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
    'A', 'B', 'C', 'D', 'E', 'F'
  };
  // if(0 < n){
  //   convert(S, n/base, base);
  //   S.push(digit[n % base]);
  // }
  while(n > 0){
    int remainder = (int)(n % base);
    S.push(digit[remainder]);
    n /= base;
  }
};

int main(){
  Stack<char> outputStack;
  convert(outputStack, 120, 16);
}