#pragma once

template <typename T>
void List<T>::copyNodes(ListNodePosi(T) p, int n){
    init();
    while(n--){
        insertAsLast(p -> data);
        p = p->succ;
    }
}

template <typename T>
List<T>::List(ListNodePosi(T) p, int n){
    copyNodes(p, n);
}

template <typename T>
List<T>::List(List<T> const& L){
    copyNodes(L.first(), L._size);
}

template <typename T>
List<T>::List(List<T> const& L, int r, int n){
    copyNodes(L[r], n);
}

template <typename T>
List<T>::List(T const* A, int n){
    init();
    for(int i = 0; i < n; i++){
        insertAsLast(A[i]);
    }
}