#pragma once

template <typename T>
int List<T>::disordered() const {
  int d = 0;
  ListNodePosi(T) p = first();
  for(int i = 0; i < _size - 1; p = p->succ, i++){
    if(p->data > p->succ->data){
      d++;
    }
  }
  return d;
}
