#pragma once

#include "list-node.h"

template <typename T> class List{
private:
    int _size;
    ListNodePosi(T) header;
    ListNodePosi(T) trailer;

protected:
    void init();
    int clear();
    void copyNodes(ListNodePosi(T), int);
    void merge(ListNodePosi(T)&, int, List<T>&, ListNodePosi(T)&, int);
    void mergeSort(ListNodePosi(T)&, int);
    void selectionSort(ListNodePosi(T)&, int);
    void insertionSort(ListNodePosi(T)&, int);

public:
    List(){ init(); }
    List(List<T> const& L);
    List(List<T> const& L, Rank r, int n);
    List(ListNodePosi(T) p, int n);
    List(T const* A, int n);

    ~List();

    Rank size() const { return _size; }
    bool empty() const { return _size <= 0; } 
    T& operator[](Rank r) const;
    ListNodePosi(T) first() const { return header->succ; }
    ListNodePosi(T) last() const { return trailer->pred; }
    bool valid(ListNodePosi(T) p) {
        return p && (trailer != p) && (header != p);
    }
    int disordered() const;
    ListNodePosi(T) find(T const& e) const{
        return find(e, _size, trailer);
    };
    ListNodePosi(T) find(T const& e, int n, ListNodePosi(T) p) const;
    ListNodePosi(T) search(T const& e) const{
        return search(e, _size, trailer);
    }
    ListNodePosi(T) search(T const& e, int n, ListNodePosi(T) p) const;
    ListNodePosi(T) selectMax(ListNodePosi(T) p, int n);
    ListNodePosi(T) selectMax() {
        return selectMax(header->succ, _size);
    }

    ListNodePosi(T) insertAsFirst(T const& e);
    ListNodePosi(T) insertAsLast(T const& e);
    ListNodePosi(T) insertAfter(ListNodePosi(T) p, T const& e);
    ListNodePosi(T) insertBefore(ListNodePosi(T) p, T const& e);
    T remove(ListNodePosi(T) p);
    void sort(ListNodePosi(T) p, int n);
    void sort(){
        sort(first(), _size);
    }
    int deduplicate();
    int uniquify();
    void reverse();

    void traverse(void (*)(T&));
    template <typename VST> void traverse(VST&);

    friend std::ostream& operator<<(std::ostream& os, const List<T>& lst){
        for(ListNodePosi(T) p = lst.header->succ; p != lst.trailer; p = p->succ){
            os << p->data << '\t';
        }
        return os;
    }
};

#include "./list-implementation/bracket.h"
#include "./list-implementation/clear.h"
#include "./list-implementation/copy-nodes.h"
#include "./list-implementation/deduplicate.h"
#include "./list-implementation/destructor.h"
#include "./list-implementation/disordered.h"
#include "./list-implementation/find.h"
#include "./list-implementation/init.h"
#include "./list-implementation/insert.h"
#include "./list-implementation/insertion_sort.h"
#include "./list-implementation/merge_sort.h"
#include "./list-implementation/remove.h"
#include "./list-implementation/search.h"
#include "./list-implementation/select_max.h"
#include "./list-implementation/selection_sort.h"
#include "./list-implementation/sort.h"
#include "./list-implementation/traverse.h"
#include "./list-implementation/uniquify.h"