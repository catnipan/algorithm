#include <iostream>
#include "./vector.h"

template <typename T> struct Print {
  virtual void operator()(T &e) {
    std::cout << e << std::endl;
  }
};

int add1(int& x){
  return x + 1;
}

int times2(const int& x){
  return x * 2;
}

struct Multiply {
  int _o;
  Multiply(int o){
    _o = o;
  }
  int operator()(int x){
    return x * _o;
  }
};

int main(){
  int listA[] = {1,56,3,23,7,10,1,1,3,24,54,9,4,8,9};
  size_t sizeListA = sizeof(listA) / sizeof(*listA);
  Vector<int> vectorA(listA, sizeListA);

  // vectorA.increaseAllByOne();
  // print(vectorA.map(add1).map(times2));

  // auto lambdaAdd1 = [](const int& x)->int{ return x + 1; };
  //   auto vectorB = vectorA.map([](const int& x)->int{ return x + 1; });
  //   print(vectorB);

  //   vectorB.sort();
  //   print(vectorB);
  // vectorA.sort();
  // print(vectorA);
  // std::cout << vectorA.search(10) << std::endl;
  std::cout << vectorA << std::endl;
  vectorA.sort();
  std::cout << vectorA << std::endl;
  // Multiply m3(3);
  // print(vectorA.map(m3));

  return 0;
}
