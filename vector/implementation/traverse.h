#pragma once

template <typename T>
void Vector<T>::traverse(void (*visit)(T&)){
  for(Rank i = 0; i < _size; i++){
    visit(_elem[i]);
  }
}

template <typename T> template <typename VST>
void Vector<T>::traverse(VST& visit){
  for(Rank i = 0; i < _size; i++){
    visit(_elem[i]);
  }
}

template <typename T> struct Increase {
  virtual void operator()(T &e) { e++; }
};

template <typename T>
void Vector<T>::increaseAllByOne(){
  Increase<T> increase;
  traverse(increase);
}

template <typename T>
Vector<T> Vector<T>::map(T (* mapFunction)(const T &)){
  T _new_elem[_size];
  for(Rank i = 0; i < _size; i++){
    _new_elem[i] = mapFunction(_elem[i]);
  }
  Vector<T> newVec(_new_elem, _size);
  return newVec;
}

template <typename T> template <typename VST>
Vector<T> Vector<T>::map(VST &mapFunction){
  T _new_elem[_size];
  for(Rank i = 0; i < _size; i++){
    _new_elem[i] = mapFunction(_elem[i]);
  }
  Vector<T> newVec(_new_elem, _size);
  return newVec;
}
